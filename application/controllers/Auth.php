<?php

/**
*
*/
class Auth extends CI_Controller
{

  public function login(){

    $this->form_validation->set_rules('username', 'Username', 'trim|required');
    $this->form_validation->set_rules('password', 'Password', 'trim|required');

    if ($this->form_validation->run() == TRUE) {

      $username = $_POST['username'];
      $password = md5($_POST['password']);

      // check user un DB
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where(array('username'=>$username, 'password'=>$password));
      $query = $this->db->get();

      $user = $query->row();
      if ($user->username) {
        // temporary message
        $this->session->set_flashdata('success', 'You are logged in.');

        // session variable
        $_SESSION['user_logged'] = TRUE;
        $_SESSION['username'] = $user->username;

        // redirect to dashboard
        redirect('admin/dashboard','refresh');
      }else{
        $this->session->set_flashdata('error', 'No such account exits.');
        redirect('auth/login','refresh');
      }
    }

    // load view
    $this->load->view('login');
  }

  public function register(){

    if(isset($_POST['register'])){
      $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required');
      $this->form_validation->set_rules('username', 'Username', 'trim|required');
      $this->form_validation->set_rules('password', 'Password', 'trim|required');
      $this->form_validation->set_rules('repassword', 're-enter Password', 'trim|required|min_length[8]|matches[password]');

      if ($this->form_validation->run() == TRUE) {
        echo 'form validated';
        // add user in databases
        // user column at db :)
        $data = array(
          'full_name'=>$_POST['fullname'],
          'username'=>$_POST['username'],
          'password'=>md5($_POST['password']),
          );
        $this->db->insert('user', $data);

        $this->session->set_flashdata('success', 'Your account has been registered. You can login now.');
        redirect('auth/register','refresh');
      }
    }

    // load view

    $this->load->view('register');
  }

  public function logout(){
    unset($_SESSION);
    session_destroy();
    redirect('auth/login','refresh');
  }
}